@echo off
setlocal

set PATH=%~dp0tools\z88dk\bin;%PATH%
set ZCCCFG=%~dp0tools\z88dk\lib\config

cd %~dp0
if errorlevel 1 exit /B 1

zcc +zx  -clib=sdcc_iy -startup=1 --list --opt-code-size -Cz"--sna" -o build\test -create-app  ^
	main.c
zcc +zx  -clib=sdcc_iy -startup=1 --list --opt-code-size -o build\test -create-app  ^
	main.c

if errorlevel 1 exit /B 1
