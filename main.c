#include <arch/zx.h>
#include <string.h>
#include <stdio.h>

__sfr __banked __at 0xfbdf MouseX; // declare a 16-bit port at i/o address 0xfbfe
__sfr __banked __at 0xffdf MouseY;
__sfr __banked __at 0xfadf MouseButtons;
__sfr __banked __at 0xf7fe key1_5;
__sfr __banked __at 0xeffe key6_0;

__sfr __banked __at 0x7ffe keySpB;
__sfr __banked __at 0xbffe keyEnH;
__sfr __banked __at 0xdffe keyP_Y;
__sfr __banked __at 0xfbfe keyQ_T;
__sfr __banked __at 0xfdfe keyA_G;
__sfr __banked __at 0xfefe keyCsV;

const char *bit_rep[16] = {
    [ 0] = "0000", [ 1] = "0001", [ 2] = "0010", [ 3] = "0011",
    [ 4] = "0100", [ 5] = "0101", [ 6] = "0110", [ 7] = "0111",
    [ 8] = "1000", [ 9] = "1001", [10] = "1010", [11] = "1011",
    [12] = "1100", [13] = "1101", [14] = "1110", [15] = "1111",
};

void print_byte(uint8_t byte)
{
  printf("%s%s", bit_rep[byte >> 4], bit_rep[byte & 0x0F]);
}

  int main()
  {
    zx_cls(PAPER_WHITE);
    for(;;)
    {
      printf("\x16\x01\x01""0xfbdf: MouseX:  %3i  ", MouseX);
      print_byte(MouseX);
      printf("\x16\x01\x02""0xffdf: MouseY:  %3i  ", MouseY);
      print_byte(MouseY);
      printf("\x16\x01\x03""0xfadf: Buttons: %3i  ", MouseButtons);
      print_byte(MouseButtons);
      printf("\x16\x01\x04""0xf7fe: key 1-5: %3i  ", key1_5);
      print_byte(key1_5);
      printf("\x16\x01\x05""0xeffe: key 6-0: %3i  ", key6_0);
      print_byte(key6_0); 
      printf("\x16\x01\x06""0x7ffe: key SpB: %3i  ", keySpB);
      print_byte(keySpB); 
      printf("\x16\x01\x07""0xbffe: key EnH: %3i  ", keyEnH);
      print_byte(keyEnH); 
      printf("\x16\x01\x08""0xdffe: key P_Y: %3i  ", keyP_Y);
      print_byte(keyP_Y); 
      printf("\x16\x01\x09""0xfbfe: key Q_T: %3i  ", keyQ_T);
      print_byte(keyQ_T); 
      printf("\x16\x01\x0a""0xfdfe: key A_G: %3i  ", keyA_G);
      print_byte(keyA_G); 
      printf("\x16\x01\x0b""0xfefe: key CSV: %3i  ", keyCsV);
      print_byte(keyCsV); 
           
    }
  }

